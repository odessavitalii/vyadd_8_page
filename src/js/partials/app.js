$(window).on('load', function () {
    setTimeout(() => {
        $('body').removeClass('is-preload');
    }, 1000);
});

$(document).ready(function () {
    init();
});

$(window).resize(function () {
    init();
});

function init() {

    const $body = $('body');
    const $window = $(window);
    const $phones = $('#phones');
    const $btnMenu = $('#btnMenu');
    const $btnMenuClose = $('#btnMenuClose');
    const $headerFixed = $('.header--fixed');
    const $slickCompany = $('#slider-company');
    const $slickHowItWork = $('#slider-how-it-work');
    const $typing = $('#typing');
    const $dropdown = $('[data-toggle="dropdown"]');
    const isAnchor = $body.hasClass('page-anchor');

    if ($typing.length) {
        const typed = new Typed('#typing', {
            strings: ["demand", "sign", "traffic"],
            smartBackspace: true, // Default value
            typeSpeed: 200,
            backDelay: 2000,
            startDelay: 1500,
            loop: true,
        });

    }

    if ($slickCompany.length) {
        $slickCompany.slick({
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            adaptiveHeight: true,
            nextArrow: `<svg class="slick-next" width="40" height="120" viewBox="0 0 40 120" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M2.76641e-07 109.825L33.2168 60L2.98602e-06 10.1748L3.26266e-06 0L40 60L0 120L2.76641e-07 109.825Z" fill="#3E4660"/>
                        </svg>`,
            prevArrow: `<svg class="slick-prev" width="40" height="120" viewBox="0 0 40 120" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M40 109.825L6.7832 60L40 10.1748L40 0L-3.8147e-06 60L40 120L40 109.825Z" fill="#3E4660"/>
                        </svg>`
        });
    }

    if ($slickHowItWork.length) {
        $slickHowItWork.slick({
            autoplay: true,
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            arrows: false
        })
    }

    $btnMenu.on('click', function () {
        $body.addClass('is-menu-open');
    });

    $btnMenuClose.on('click', function () {
        $body.removeClass('is-menu-open');
    });

    if ($window.width() <= 767) {

        if ($phones.length) {
            $phones.slick({
                autoplay: true,
                infinite: true,
                speed: 300,
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
                arrows: false
            })
        }
    }

    $dropdown.on("click", function(event) {

        const $this = $(this);
        const target = event.target;
        const $dropdownToggle = $this.find('.dropdown-toggle');
        const $dropdownItems = $this.find('.dropdown-item');
        const $dropdownMenu = $this.find('.dropdown-menu');

        $this.toggleClass("show");

        if ($window.width() > 767) {

            if ($this.hasClass('open')) {
                $this.removeClass('open');
            }

            if (target.className === 'dropdown-item') {

                if (isAnchor) {
                    event.preventDefault();
                }

                let text = $(target).text();
                let url = $(target).attr('href');

                $dropdownItems.removeClass('hidden');
                $(target).addClass('hidden');

                $dropdownToggle.text(text);
                $dropdownToggle.attr('href', url);
                location = url;

                $this.removeClass("show");
            }

        } else {

            if (target.className === 'dropdown-item') {

                if (isAnchor) {
                    event.preventDefault();
                }

                let text = $(target).text();
                let url = $(target).attr('href');

                $dropdownItems.removeClass('hidden');
                $(target).addClass('hidden');

                $dropdownToggle.text(text);
                $dropdownToggle.attr('href', url);
                location = url;

                $this.removeClass("show");
            }

            if ($this.hasClass('open')) {

                $this.removeClass('open');
                $dropdownMenu.slideUp();

            } else {

                $this.addClass('open');
                $dropdownMenu.slideDown();

            }

        }
    });

    $(document).on('scroll', function() {

        if ($headerFixed.length) {
            if ($window.scrollTop() > 0) {
                $headerFixed.addClass('sticky')
            } else {
                $headerFixed.removeClass('sticky')
            }
        }
    });

}
